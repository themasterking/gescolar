package org.view.stageConnexion.firstConnexion;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.Langue;
import org.LangueBase;
import org.model.Admin;

import java.net.URL;
import java.util.ResourceBundle;

public class ProfilViewController implements Initializable, LangueBase {

    @FXML
    private AnchorPane sceneProfilPane;
    @FXML
    private Label name_user;
    @FXML
    private Label small_profil;
    @FXML
    private Label big_profil;
    @FXML
    private Label lien_profil;
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private TextField tel;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField confirmPassword;
    @FXML
    private ImageView photo_profil;

    private static String nomProfil, prenomProfil, emailProfil, usernameProfil, passwordProfil, confirmPasswordProfil, photoProfil;
    private static int telProfil;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        //GeneralViewController.getPane0().setVisible(false);
        //GeneralViewController.getPane1().setVisible(true);

        name_user.setText(Admin.getUsername());
        loadLang(Langue.getLangue());
    }

    @Override
    public void loadLang (String lang) {
        Langue.setLangue(lang);
        langue.changeText(small_profil, "small_profil");
        langue.changeText(big_profil, "big_profil");
        langue.changeText(lien_profil, "lien_profil");
        langue.changeText(nom, "nom");
        langue.changeText(prenom, "prenom");
        langue.changeText(email, "email");
        langue.changeText(tel, "tel");
        langue.changeText(username, "username");
        langue.changeText(password, "password");
        langue.changeText(confirmPassword, "confirmPassword");
    }

    @FXML
    public void remplirChamp(KeyEvent event){
        if (event.getSource() == nom){
            nomProfil = nom.getText();
        }
        if (event.getSource() == prenom){
            prenomProfil = prenom.getText();
        }
        if (event.getSource() == email){
            emailProfil = email.getText();
        }
        if (event.getSource() == tel){
            telProfil = Integer.parseInt(tel.getText());
        }
        if (event.getSource() == username){
            usernameProfil = username.getText();
        }
        if (event.getSource() == password){
            passwordProfil = password.getText();
        }
    }

    public static boolean successUpdate (){
        System.out.println(nomProfil + " " + prenomProfil + " " + usernameProfil + " " + passwordProfil + " " + telProfil + " " + emailProfil);
        return Admin.update(nomProfil, prenomProfil, usernameProfil, passwordProfil, telProfil, "", emailProfil);
    }

    public static String getNomProfil () {
        return nomProfil;
    }

    public static void setNomProfil (String nomProfil) {
        ProfilViewController.nomProfil = nomProfil;
    }

    public static String getPrenomProfil () {
        return prenomProfil;
    }

    public static void setPrenomProfil (String prenomProfil) {
        ProfilViewController.prenomProfil = prenomProfil;
    }

    public static String getEmailProfil () {
        return emailProfil;
    }

    public static void setEmailProfil (String emailProfil) {
        ProfilViewController.emailProfil = emailProfil;
    }

    public static String getUsernameProfil () {
        return usernameProfil;
    }

    public static void setUsernameProfil (String usernameProfil) {
        ProfilViewController.usernameProfil = usernameProfil;
    }

    public static String getPasswordProfil () {
        return passwordProfil;
    }

    public static void setPasswordProfil (String passwordProfil) {
        ProfilViewController.passwordProfil = passwordProfil;
    }

    public static String getConfirmPasswordProfil () {
        return confirmPasswordProfil;
    }

    public static void setConfirmPasswordProfil (String confirmPasswordProfil) {
        ProfilViewController.confirmPasswordProfil = confirmPasswordProfil;
    }

    public static String getPhotoProfil () {
        return photoProfil;
    }

    public static void setPhotoProfil (String photoProfil) {
        ProfilViewController.photoProfil = photoProfil;
    }

    public static int getTelProfil () {
        return telProfil;
    }

    public static void setTelProfil (int telProfil) {
        ProfilViewController.telProfil = telProfil;
    }
}
