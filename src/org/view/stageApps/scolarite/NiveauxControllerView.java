package org.view.stageApps.scolarite;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.CycleTable;
import org.model.NiveauTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class NiveauxControllerView implements Initializable {

    /////////////////// Gestion des tableaux de niveau ///////////////////////////
    @FXML
    private ComboBox<String> cycleCombox;
    @FXML
    private TableView<NiveauTable> niveauTableTableView;
    @FXML
    private TableColumn<NiveauTable, String> col_numero;
    @FXML
    private TableColumn<NiveauTable, String> col_nom;

    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField ajoutDeNiveau;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;

    private static ObservableList<NiveauTable> niveauTables = FXCollections.observableArrayList();
    private static boolean updateBtn=false;
    private static int idCycle;


    @FXML
    public void action(MouseEvent event){
        int selectedIndex = niveauTableTableView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun niveau sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un niveau");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                niveauTableTableView.getSelectionModel().selectedItemProperty().getValue().remove();
                obtenirNiveau();
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    //niveauTableTableView.setEditable(true);
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    //niveauTableTableView.setEditable(false);
                    ///niveauTableTableView.getSelectionModel().selectedItemProperty().getValue().update(nomCycleTextField.getText());
                    obtenirNiveau();
                }
            }

            if (event.getSource() == addIcon || event.getSource() == addCircle){
                new NiveauTable().add(ajoutDeNiveau.getText(), idCycle);
                ajoutDeNiveau.setText(null);
                obtenirNiveau();
            }
        }

    }

    @FXML
    private void selectCycle(ActionEvent event){
        if (event.getSource() == cycleCombox){
            obtenirNiveau();
        }
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        obtenirCycle();
    }

    public void obtenirCycle(){
        Connection con = null;
        ResultSet resultSet = null;
        try {
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT DISTINCT * FROM cycle ORDER BY cycle_nom;");
            niveauTables.clear();
            int i=0;
            ObservableList<String> cycleTable = FXCollections.observableArrayList();
            while (resultSet.next()){
                CycleTable cycle = new CycleTable(i+1, resultSet.getString("cycle_nom"));
                cycleTable.add(cycle.getNom());
                i++;
            }
            cycleCombox.setItems(cycleTable);
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    public void obtenirNiveau(){
        System.out.println(cycleCombox.getValue());
        Connection con = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            obtenirCycle();
            con = ConnexionUtils.conDB();
            preparedStatement = con.prepareStatement("SELECT DISTINCT * FROM cycle WHERE cycle.cycle_nom=?;");
            preparedStatement.setString(1, cycleCombox.getValue());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                idCycle = resultSet.getInt("id_cycle");
            }
            preparedStatement = con.prepareStatement("SELECT DISTINCT * FROM niveau, cycle WHERE niveau.id_cycle=cycle.id_cycle AND cycle.cycle_nom=?;");
            preparedStatement.setString(1, cycleCombox.getValue());
            resultSet = preparedStatement.executeQuery();
            niveauTables.clear();
            int i=0;

            while (resultSet.next()){
                NiveauTable niveau = new NiveauTable(i+1, resultSet.getString("niveau_nom"));
                niveau.setId(resultSet.getInt("niveau.id_niveau"));
                niveauTables.add(niveau);
                i++;
            }
            System.out.println(idCycle);
        }catch (SQLException e){
            e.printStackTrace();
        }

        col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        niveauTableTableView.setItems(niveauTables);
    }
}
