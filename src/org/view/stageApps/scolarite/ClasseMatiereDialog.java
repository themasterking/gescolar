package org.view.stageApps.scolarite;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.MatiereTable;
import org.model.StudentTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class ClasseMatiereDialog implements Initializable {

    /////////////////// Gestion des tableaux de matiere ///////////////////////////
    @FXML
    private TableView<MatiereTable> matiereTableTableView;
    @FXML
    private TableColumn<MatiereTable, String> col_numero;
    @FXML
    private TableColumn<MatiereTable, String> col_nom;
    @FXML
    private TableColumn<MatiereTable, String> col_credit;

    @FXML
    private ComboBox<String> ajoutMatiereCombobox;
    @FXML
    private Button closeOK;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;

    private static ObservableList<String> matiereTablesUtilisee = FXCollections.observableArrayList();
    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    @FXML
    public void action(ActionEvent event){

        if (event.getSource() == closeOK)
            MainApp.getDialogueStage().close();

    }

    @FXML
    public void actionMouse(MouseEvent event){
        int selectedIndex = matiereTableTableView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun matiere sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un matiere");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                System.out.println("Supprimé");
                MatiereTable matiere = matiereTableTableView.getSelectionModel().getSelectedItem();
                try {
                    con = ConnexionUtils.conDB();
                    matiereTablesUtilisee.remove(matiere.getNom());
                    preparedStatement = con.prepareStatement("DELETE FROM avoir_matiere_classe WHERE id_matiere=? AND id_classe=?;");
                    preparedStatement.setInt(1, matiere.getId());
                    preparedStatement.setInt(2, ClasseControllerView.getValueClasse().getId());
                    preparedStatement.execute();
                }catch (Exception e){
                    e.printStackTrace();
                }
                initialize(null, null);
            }
        }

        if (event.getSource() == addIcon || event.getSource() == addCircle){
            System.out.println("Ajouté");
            try {
                con = ConnexionUtils.conDB();
                matiereTablesUtilisee.add(ajoutMatiereCombobox.getValue());
                preparedStatement = con.prepareStatement("SELECT DISTINCT id_matiere FROM matiere WHERE matiere_nom=?;");
                preparedStatement.setString(1, ajoutMatiereCombobox.getValue());
                resultSet = preparedStatement.executeQuery();
                int idMat=0;
                while (resultSet.next()){
                    idMat = resultSet.getInt("id_matiere");
                }
                preparedStatement = con.prepareStatement("INSERT INTO avoir_matiere_classe(id_matiere, id_classe) VALUES(?, ?);");
                preparedStatement.setInt(1, idMat);
                preparedStatement.setInt(2, ClasseControllerView.getValueClasse().getId());
                preparedStatement.executeUpdate();
                initialize(null, null);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        //System.out.println(ClasseControllerView.getValueClasse().getNom());
        listeMatiereClasse();
        listeAllMatieres();
    }

    public void listeAllMatieres(){
        try {
            ObservableList<String> matiere = FXCollections.observableArrayList();
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT matiere_nom FROM matiere;");
            while (resultSet.next()){
                String mat = resultSet.getString("matiere_nom");
                if (!matiereTablesUtilisee.contains(mat))
                    matiere.add(mat);
            }
            ajoutMatiereCombobox.setItems(matiere);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void listeMatiereClasse(){
        try {
            ObservableList<MatiereTable> matiereTables = FXCollections.observableArrayList();
            con = ConnexionUtils.conDB();
            String sql = "SELECT matiere.* FROM classe, matiere, avoir_matiere_classe " +
                    "WHERE avoir_matiere_classe.id_matiere = matiere.id_matiere AND avoir_matiere_classe.id_classe =classe.id_classe AND classe.classe_nom=?;";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, ClasseControllerView.getValueClasse().getNom());
            resultSet = preparedStatement.executeQuery();
            matiereTables.clear();
            int i=0;

            while (resultSet.next()){
                MatiereTable matiere = new MatiereTable(i+1, resultSet.getString("matiere_nom"), resultSet.getInt("matiere_credit"));
                matiere.setId(resultSet.getInt("id_matiere"));
                matiereTables.add(matiere);
                matiereTablesUtilisee.add(matiere.getNom());
                i++;
            }

            col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
            col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
            col_credit.setCellValueFactory(new PropertyValueFactory<>("credit"));
            matiereTableTableView.setItems(matiereTables);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
