package org.view.stageApps.ecole;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.StudentTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

//import javafx.scene.image.ImageView;

public class SalleControllerView implements Initializable {

    /////////////////// Gestion des tableaux d'étudiant ///////////////////////////
    @FXML
    private TableView<StudentTable> etudiantsTreeView;
    @FXML
    private TableColumn<StudentTable, String> col_numero;
    @FXML
    private TableColumn<StudentTable, String> col_nom;
    @FXML
    private TableColumn<StudentTable, String> col_prenom;


    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField nomEtudiantTextField;
    @FXML
    private TextField prenomEtudiantTextField;
    @FXML
    private TextField matriculeEtudiantTextField;
    @FXML
    private TextField cniEtudiantTextField;
    @FXML
    private DatePicker dateNaissEtudiantTextField;
    @FXML
    private TextField lieuNaissEtudiantTextField;
    @FXML
    private TextField sexeEtudiantTextField;
    @FXML
    private TextField paysEtudiantTextField;
    @FXML
    private TextField telEtudiantTextField;
    @FXML
    private TextField emailEtudiantTextField;
    @FXML
    private TextField faxEtudiantTextField;
    @FXML
    private TextField villeEtudiantTextField;
    @FXML
    private Label classeEtudiantLabel;
    @FXML
    private FontAwesomeIconView ppEtudiantLabel;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;




    private static ObservableList<StudentTable> studentTables = FXCollections.observableArrayList();
    private static boolean updateBtn=false;


    @FXML
    public void action(MouseEvent event){
        int selectedIndex = etudiantsTreeView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun étudiant sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un étudiant");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                etudiantsTreeView.getSelectionModel().selectedItemProperty().getValue().remove();
                initialize(null, null);
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    showUpdateEtudiant(updateBtn);
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    showUpdateEtudiant(updateBtn);
                    etudiantsTreeView.getSelectionModel().selectedItemProperty().getValue().update(
                            nomEtudiantTextField.getText(),
                            prenomEtudiantTextField.getText(),
                            cniEtudiantTextField.getText(),
                            Date.valueOf(dateNaissEtudiantTextField.getValue()),
                            lieuNaissEtudiantTextField.getText(),
                            emailEtudiantTextField.getText(),
                            Integer.parseInt(telEtudiantTextField.getText()),
                            sexeEtudiantTextField.getText(),
                            villeEtudiantTextField.getText(),
                            ppEtudiantLabel.getText(),
                            faxEtudiantTextField.getText(),
                            paysEtudiantTextField.getText(),
                            matriculeEtudiantTextField.getText()
                    );
                    initialize(null, null);
                }
            }

            if (event.getSource() == addIcon || event.getSource() == addCircle){
                MainApp.dialoguesStage("./view/stageApps/scolarite/EtudiantDialogView.fxml", "Ajouter un nouveau élève");
                initialize(null, null);
            }
        }

    }


    @Override
    public void initialize (URL location, ResourceBundle resources) {
        Connection con = null;
        ResultSet resultSet = null;

        try {
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT DISTINCT * FROM etudiant ORDER BY etudiant_nom, etudiant_prenom;");
            studentTables.clear();
            int i=0;

            while (resultSet.next()){
                StudentTable student = new StudentTable(i+1,
                        resultSet.getString("etudiant_nom"),
                        resultSet.getString("etudiant_prenom"));
                student.setId(resultSet.getInt("id_etudiant"));
                student.setCni(resultSet.getString("etudiant_cni"));
                student.setDate_naiss(resultSet.getDate("etudiant_date_naiss"));
                student.setLieu_naiss(resultSet.getString("etudiant_lieu_naiss"));
                student.setEmail(resultSet.getString("etudiant_email"));
                student.setTel(resultSet.getInt("etudiant_tel"));
                student.setSexe(resultSet.getString("etudiant_sexe"));
                student.setVille(resultSet.getString("etudiant_ville"));
                student.setPp(resultSet.getString("etudiant_pp"));
                student.setFax(resultSet.getString("etudiant_fax"));
                student.setPays(resultSet.getString("etudiant_pays"));
                student.setMatricule(resultSet.getString("etudiant_matricule"));
                student.setUsername(resultSet.getString("etudiant_username"));
                student.setPassword(resultSet.getString("etudiant_password"));
                studentTables.add(student);
                i++;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        col_prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        etudiantsTreeView.setItems(studentTables);

        showStudent(null);
        etudiantsTreeView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showStudent(newValue));
    }


    public void showStudent(StudentTable studentTable){
        if (studentTable != null){
            nomEtudiantTextField.setText(studentTable.getNom());
            prenomEtudiantTextField.setText(studentTable.getPrenom());
            matriculeEtudiantTextField.setText(studentTable.getMatricule());
            cniEtudiantTextField.setText(studentTable.getCni());
            dateNaissEtudiantTextField.setValue(studentTable.getDate_naiss().toLocalDate());
            lieuNaissEtudiantTextField.setText(studentTable.getLieu_naiss());
            sexeEtudiantTextField.setText(studentTable.getSexe());
            paysEtudiantTextField.setText(studentTable.getPays());
            telEtudiantTextField.setText(Integer.toString(studentTable.getTel()));
            emailEtudiantTextField.setText(studentTable.getEmail());
            faxEtudiantTextField.setText(studentTable.getFax());
            villeEtudiantTextField.setText(studentTable.getVille());
            classeEtudiantLabel.setText("INGE 3");
            //ppEtudiantTextField;
        }else {
            nomEtudiantTextField.setText("");
            prenomEtudiantTextField.setText("");
            matriculeEtudiantTextField.setText("");
            cniEtudiantTextField.setText("");
            //dateNaissEtudiantTextField.setValue(null);
            lieuNaissEtudiantTextField.setText("");
            sexeEtudiantTextField.setText("");
            paysEtudiantTextField.setText("");
            telEtudiantTextField.setText("");
            emailEtudiantTextField.setText("");
            faxEtudiantTextField.setText("");
            villeEtudiantTextField.setText("");
            classeEtudiantLabel.setText("");
        }
    }

    public void showUpdateEtudiant(boolean cond){
        nomEtudiantTextField.setEditable(cond);
        prenomEtudiantTextField.setEditable(cond);
        matriculeEtudiantTextField.setEditable(cond);
        cniEtudiantTextField.setEditable(cond);
        dateNaissEtudiantTextField.setDisable(!cond);
        lieuNaissEtudiantTextField.setEditable(cond);
        sexeEtudiantTextField.setEditable(cond);
        paysEtudiantTextField.setEditable(cond);
        telEtudiantTextField.setEditable(cond);
        emailEtudiantTextField.setEditable(cond);
        faxEtudiantTextField.setEditable(cond);
        villeEtudiantTextField.setEditable(cond);
    }

    public static ObservableList<StudentTable> getStudentTables () {
        return studentTables;
    }

    public static void setStudentTables (ObservableList<StudentTable> studentTables) {
        SalleControllerView.studentTables = studentTables;
    }
}
