package org.model;

import java.sql.SQLException;

public class ExamenTable extends InterActionBD {

    private int id, num, pourcentage;
    private String nom;

    public ExamenTable () {
    }

    public ExamenTable (int num, String nom, int pourcentage) {
        this.num = num;
        this.nom = nom;
        this.pourcentage = pourcentage;
    }

    public ExamenTable (String nom, int pourcentage) {
        this.nom = nom;
        this.pourcentage = pourcentage;
    }


    public boolean add (String newNom, int newPourcentage) {
        try {
            requete("INSERT INTO examen (examen_nom, examen.examen_pourcentage) VALUES(?, ?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setInt(2, newPourcentage);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom, String newPourcentage) {
        try {
            requete("UPDATE examen SET examen_nom=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(int periode){
        try {
            requete("DELETE FROM avoir_examen_periode WHERE id_examen=? AND  id_periode=?");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().setInt(2, periode);
            super.getPreparedStatement().execute();
            requete("DELETE FROM examen WHERE id_examen=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }

    public int getPourcentage () {
        return pourcentage;
    }

    public void setPourcentage (int pourcentage) {
        this.pourcentage = pourcentage;
    }

    @Override
    public void info () throws SQLException {

    }
}

