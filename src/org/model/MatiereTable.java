package org.model;

import java.sql.SQLException;

public class MatiereTable extends InterActionBD {

    private int id, num, credit;
    private String nom;

    public MatiereTable () {
    }

    public MatiereTable (int num, String nom, int credit) {
        this.num = num;
        this.nom = nom;
        this.credit = credit;
    }


    public boolean add (String newNom, int newCredit) {
        try {
            requete("INSERT INTO matiere (matiere_nom, matiere_credit) VALUES(?, ?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setInt(2, newCredit);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom) {
        try {
            requete("UPDATE matiere SET matiere_nom=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(){
        try {
            requete("DELETE FROM matiere WHERE id_matiere=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }

    public int getCredit () {
        return credit;
    }

    public void setCredit (int credit) {
        this.credit = credit;
    }

    @Override
    public void info () throws SQLException {

    }
}
