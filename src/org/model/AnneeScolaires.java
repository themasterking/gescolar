package org.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AnneeScolaires extends InterActionBD {

    private static int id, id_ecole;
    private static Date date_debut, date_Fin;

    public AnneeScolaires () {
        try {
            info();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void info () throws SQLException {
        requete("SELECT anneescolaire.* FROM anneescolaire INNER JOIN ecole ON anneescolaire.id_ecole=?;");
        super.getPreparedStatement().setInt(1, Admin.getid_ecole());
        ResultSet resultSet = super.getPreparedStatement().executeQuery();
        while (resultSet.next()){
            id = resultSet.getInt("id_annee_scolaire");
            id_ecole = resultSet.getInt("anneeScolaire.id_ecole");
            date_debut = resultSet.getDate("anneeScolaire_dateDebut");
            date_Fin = resultSet.getDate("anneeScolaire_dateFin");
        }
    }

    public boolean insert(Date dateDebut, Date dateFin){
        try {
            requete("INSERT INTO anneescolaire (anneeScolaire_dateDebut, anneeScolaire_dateFin, anneescolaire.id_ecole) VALUES(?, ?, 1)");
            super.getPreparedStatement().setDate(1, dateDebut);
            super.getPreparedStatement().setDate(2, dateFin);
            super.getPreparedStatement().execute();
            info();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    public static int getId () {
        return id;
    }

    public static void setId (int id) {
        AnneeScolaires.id = id;
    }

    public static int getId_ecole () {
        return id_ecole;
    }

    public static void setId_ecole (int id_ecole) {
        AnneeScolaires.id_ecole = id_ecole;
    }

    public static Date getDate_debut () {
        return date_debut;
    }

    public static void setDate_debut (Date date_debut) {
        AnneeScolaires.date_debut = date_debut;
    }

    public static Date getDate_Fin () {
        return date_Fin;
    }

    public static void setDate_Fin (Date date_Fin) {
        AnneeScolaires.date_Fin = date_Fin;
    }
}
