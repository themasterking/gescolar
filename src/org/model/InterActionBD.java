package org.model;

import org.model.utils.ConnexionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class InterActionBD {

    private Connection con = null;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    public abstract void info() throws SQLException;

    public void  requete (String sql) {
        try {
            con = ConnexionUtils.conDB();
            if (con != null) {
                this.preparedStatement = con.prepareStatement(sql);
            }
        }catch (Exception e){
            this.preparedStatement = null;
        }
    }


    public Connection getCon () {
        return con;
    }

    public void setCon (Connection con) {
        this.con = con;
    }

    public PreparedStatement getPreparedStatement () {
        return preparedStatement;
    }

    public void setPreparedStatement (PreparedStatement preparedStatement) {
        this.preparedStatement = preparedStatement;
    }

    public ResultSet getResultSet () {
        return resultSet;
    }

    public void setResultSet (ResultSet resultSet) {
        this.resultSet = resultSet;
    }


}
