package org.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class School extends InterActionBD {
    private static String nom, abrege, pp, email, autorisation, adresse, fax, region, ville, pays, legale;
    private static Directeur directeur;
    private static int id, tel1, tel2, id_directeur;
    private static Date date_delivrance;

    public School () {
        try {
            info();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void info() throws SQLException {
        requete("SELECT ecole.* FROM ecole, admin WHERE admin.admin_username = ? AND admin.admin_password = ?;");
        super.getPreparedStatement().setString(1, Admin.getUsername());
        super.getPreparedStatement().setString(2, Admin.getPassword());
        ResultSet resultSet = super.getPreparedStatement().executeQuery();
        while (resultSet.next()){
            id = resultSet.getInt("id_ecole");
            nom = resultSet.getString("ecole_nom");
            abrege = resultSet.getString("ecole_abrege");
            id_directeur = resultSet.getInt("ecole.id_directeur");
            pp = resultSet.getString("ecole_logo");
            tel1 = resultSet.getInt("ecole_tel1");
            tel2 = resultSet.getInt("ecole_tel2");
            email = resultSet.getString("ecole_email");
            autorisation = resultSet.getString("ecole_autorisation");
            date_delivrance = resultSet.getDate("ecole_dateDelivrance");
            adresse = resultSet.getString("ecole_adresse");
            fax = resultSet.getString("ecole_fax");
            region = resultSet.getString("ecole_region");
            ville = resultSet.getString("ecole_ville");
            pays = resultSet.getString("ecole_pays");
            legale = resultSet.getString("ecole_legale");
        }

    }

    public boolean update (String nom, String directeur, String email, int tel1, String adresse,
                        String region, String ville, String pays) {
        try {
            requete("UPDATE ecole, directeur SET ecole.ecole_nom=?, directeur.directeur_nom=?, ecole.ecole_email=?, " +
                    "ecole.ecole_tel1=?, ecole.ecole_adresse=?, ecole.ecole_pays=?, " +
                    "ecole.ecole_region=?, ecole.ecole_ville=? WHERE directeur.id_directeur=ecole.id_ecole AND ecole.id_ecole=?;");
            super.getPreparedStatement().setString(1, nom);
            super.getPreparedStatement().setString(3, email);
            super.getPreparedStatement().setString(2, directeur);
            super.getPreparedStatement().setString(5, adresse);
            super.getPreparedStatement().setString(6, pays);
            super.getPreparedStatement().setString(7, region);
            super.getPreparedStatement().setString(8, ville);
            super.getPreparedStatement().setInt(4, tel1);
            super.getPreparedStatement().setInt(9, Admin.getid_ecole());
            super.getPreparedStatement().executeUpdate();
            info();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    /*public void update (String nom, String abrege, String pp, int tel1, int tel2, String email, String autorisation, Date date_delivrance,
    String adresse, String fax, String region, String ville, String pays, String legale) throws SQLException {
        requete("UPDATE ecole SET nom=?, abrege=?, email=?, tel1=?, tel2=?, pays=?, ville=?, region=?, " +
                "delivrance=?, autorisation=?, adresse=?, fax=?, legale=?, pp=? FROM ecole, admin WHERE admin.username = ? AND admin.PASSWORD = ?;");
        super.getPreparedStatement().setString(2, abrege);
        super.getPreparedStatement().setInt(5,tel2 );
        super.getPreparedStatement().setString(3, email);
        super.getPreparedStatement().setInt(4, tel1);
        super.getPreparedStatement().setString(1, nom);
        super.getPreparedStatement().setString(6, pays);
        super.getPreparedStatement().setString(7, ville);
        super.getPreparedStatement().setString(8, region);
        super.getPreparedStatement().setDate(9, date_delivrance);
        super.getPreparedStatement().setString(10, autorisation);
        super.getPreparedStatement().setString(11, adresse);
        super.getPreparedStatement().setString(12, fax);
        super.getPreparedStatement().setString(13, ville);
        super.getPreparedStatement().setString(14, legale);
        super.getPreparedStatement().setString(15, );
        super.getPreparedStatement().executeUpdate();
        info();
    }*/


    public static int getId () {
        return id;
    }

    public static void setId (int id) {
        School.id = id;
    }

    public static String getNom () {
        return nom;
    }

    public static void setNom (String nom) {
        School.nom = nom;
    }

    public static String getAbrege () {
        return abrege;
    }

    public static void setAbrege (String abrege) {
        School.abrege = abrege;
    }

    public static String getPp () {
        return pp;
    }

    public static void setPp (String pp) {
        School.pp = pp;
    }

    public static String getEmail () {
        return email;
    }

    public static void setEmail (String email) {
        School.email = email;
    }

    public static String getAutorisation () {
        return autorisation;
    }

    public static void setAutorisation (String autorisation) {
        School.autorisation = autorisation;
    }

    public static String getAdresse () {
        return adresse;
    }

    public static void setAdresse (String adresse) {
        School.adresse = adresse;
    }

    public static String getFax () {
        return fax;
    }

    public static void setFax (String fax) {
        School.fax = fax;
    }

    public static String getRegion () {
        return region;
    }

    public static void setRegion (String region) {
        School.region = region;
    }

    public static String getVille () {
        return ville;
    }

    public static void setVille (String ville) {
        School.ville = ville;
    }

    public static String getPays () {
        return pays;
    }

    public static void setPays (String pays) {
        School.pays = pays;
    }

    public static String getLegale () {
        return legale;
    }

    public static void setLegale (String legale) {
        School.legale = legale;
    }

    public static Directeur getDirecteur () {
        return directeur;
    }

    public static void setDirecteur (Directeur directeur) {
        School.directeur = directeur;
    }

    public static int getTel1 () {
        return tel1;
    }

    public static void setTel1 (int tel1) {
        School.tel1 = tel1;
    }

    public static int getTel2 () {
        return tel2;
    }

    public static void setTel2 (int tel2) {
        School.tel2 = tel2;
    }

    public static int getId_directeur () {
        return id_directeur;
    }

    public static void setId_directeur (int id_directeur) {
        School.id_directeur = id_directeur;
    }

    public static Date getDate_delivrance () {
        return date_delivrance;
    }

    public static void setDate_delivrance (Date date_delivrance) {
        School.date_delivrance = date_delivrance;
    }
}
