package org;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.model.School;
import org.view.stageApps.ecole.CycleControllerView;
import org.view.stageConnexion.firstConnexion.GeneralViewController;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

public class MainApp extends Application implements Serializable {

    private static Stage stageLogin, stageApps, dialogueStage;
    private static BorderPane generalLayout;
    private static Scene scene;
    private static int nbre_connexion=0;
    private int nbre_connexionSave;
    private static boolean apps;


    public static Stage getStageApps () {
        return stageApps;
    }

    public static void setStageApps (Stage stageApps) {
        MainApp.stageApps = stageApps;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Save.read();
        stageApps = new Stage();
        stageLogin = primaryStage;
        stageLogin.initStyle(StageStyle.UNDECORATED);
        //sceneLogin();
        sceneApps();
        /*System.out.println(System.getProperties().toString());
        Save.personalFolder();*/
    }

    // Page de Login
    public static void sceneLogin(){
        try {
            apps = false;
            Parent root = FXMLLoader.load(MainApp.class.getResource("./view/stageConnexion/LoginView.fxml"));
            scene = new Scene(root);
            stageLogin.setScene(scene);
            stageLogin.show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void sceneApps(){
        apps = true;
        stageApps = new Stage();
        stageApps.initStyle(StageStyle.DECORATED);
        //stageApps.initModality(Modality.APPLICATION_MODAL);
        //stageApps.setMaximized(true);
        stageApps.setTitle("GeScolar");
        stageApps.getIcons().add(new Image("org/model/assets/data/images/logos/logo.png"));
        //stageApps.setMinWidth(1200);
        try {
            Parent root = FXMLLoader.load(MainApp.class.getResource("./view/stageApps/GeneralView.fxml"));
            scene = new Scene(root);
            stageApps.setScene(scene);
            stageApps.show();
            stageLogin.close();
            new School().info();
            sceneStage("./view/stageApps/accueil/NotificationsView.fxml");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Page Générale pour La première configuration
    public static void generalFirstConnexion(){
        try {
            FXMLLoader load = new FXMLLoader();
            load.setLocation(MainApp.class.getResource("./view/stageConnexion/firstConnexion/GeneralView.fxml"));
            generalLayout = (BorderPane) load.load();
            scene = new Scene(generalLayout);
            stageLogin.setScene(scene);
            stageLogin.show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void generalApps(){
        try {
            FXMLLoader load = new FXMLLoader();
            load.setLocation(MainApp.class.getResource("./view/stageApps/GeneralView.fxml"));
            generalLayout = (BorderPane) load.load();
            scene = new Scene(generalLayout);
            stageApps.setScene(scene);
            stageApps.show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public static void sceneStage(String cheminView){
        try {
            if (apps)
                generalApps();
            else
                generalFirstConnexion();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource(cheminView));
            AnchorPane layout = (AnchorPane) loader.load();

            generalLayout.setCenter(layout);
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public static void sceneStage2(String cheminView){
        try {
            System.out.println(apps);
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource(cheminView));
            AnchorPane layout = (AnchorPane) loader.load();

            generalLayout.setCenter(layout);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void dialoguesStage(String chemin, String titreMesg){
        dialogueStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource(chemin));
            AnchorPane page = (AnchorPane) loader.load();
            Scene scene = new Scene(page);

            dialogueStage.setTitle(titreMesg);
            dialogueStage.initModality(Modality.WINDOW_MODAL);
            dialogueStage.initOwner(stageApps);
            dialogueStage.setResizable(false);
            dialogueStage.setScene(scene);

            dialogueStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Stage getStageLogin () {
        return stageLogin;
    }

    public static void setStageLogin (Stage stageLogin) {
        MainApp.stageLogin = stageLogin;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static Scene getScene () {
        return scene;
    }

    public static void setScene (Scene scene) {
        MainApp.scene = scene;
    }

    public static int getNbre_connexion () {
        return nbre_connexion;
    }

    public static void setNbre_connexion (int nbre_connexion) {
        MainApp.nbre_connexion = nbre_connexion;
    }

    public static BorderPane getGeneralLayout () {
        return generalLayout;
    }

    public static void setGeneralLayout (BorderPane generalLayout) {
        MainApp.generalLayout = generalLayout;
    }

    public static Stage getDialogueStage () {
        return dialogueStage;
    }

    public static void setDialogueStage (Stage dialogueStage) {
        MainApp.dialogueStage = dialogueStage;
    }

    public static boolean isApps () {
        return apps;
    }

    public static void setApps (boolean apps) {
        MainApp.apps = apps;
    }

    public int getNbre_connexionSave () {
        return nbre_connexionSave;
    }

    public void setNbre_connexionSave (int nbre_connexionSave) {
        this.nbre_connexionSave = nbre_connexionSave;
    }
}
